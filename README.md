# FiguresLand
##### Proyecto para la materia de Graficación.

Use las flechas para moverse por la simulación.
Cuando esté cerca de una figura puede usar el mouse para rotarla.

Puede encontrar el código del proyecto en el siguiente repositorio:
https://gitlab.com/FDanielGomez/figuresland.git